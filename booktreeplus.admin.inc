<?php

/**
 * @file
 * Administrative page callbacks for the booktreeplus module.
 */

/**
 * Administration settings form.
 */
function booktreeplus_admin_settings($form, &$form_state) {
  $params = _booktreeplus_params(NULL);
  $form['booktreeplus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Booktree Plus default settings'),
    '#weight' => -5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  _booktreeplus_build_form($form['booktreeplus'], 'booktreeplus--', $params);
  return system_settings_form($form);
}
