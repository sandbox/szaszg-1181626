(function ($) {

Drupal.behaviors.booktreeplus = {

  attach: function(context, settings) {

    // Collapse the sub-folders.
    $('.booktreeplus .pages ul').hide();

    // Check iconified state
    if (Drupal.settings.booktreeplus['cookie']) {
      icon = $.cookie('booktreeplus-' + Drupal.settings.booktreeplus['bid'] + '-icon');
      if (icon == 'Yes') {
        nav = $('#booktreeplus-' + Drupal.settings.booktreeplus['bid']);
        $(nav).find('img.booktreeplus-navigation').show();
        $(nav).find('div.booktreeplus-navigation').hide();
      }
      else if (icon == 'No') {
        nav = $('#booktreeplus-' + Drupal.settings.booktreeplus['bid']);
        $(nav).find('img.booktreeplus-navigation').hide();
        $(nav).find('div.booktreeplus-navigation').show();
      }
    }
    if (Drupal.settings.booktreeplus['expand']) {
    // Expand all if necc.
      var expand;
      if (Drupal.settings.booktreeplus['cookie'])
        expand = $.cookie('booktreeplus-' + Drupal.settings.booktreeplus['bid'] + '-expand');
      if (expand != 'Yes')
        expand = Drupal.settings.booktreeplus['expand'] ? 'Yes' : 'No';
      nav = $('#booktreeplus-' + Drupal.settings.booktreeplus['bid']);
      if( expand == 'Yes' )
        $(nav).find('.pages li:has(ul)').addClass('expanded').find('ul:first').show();
//      else
//        $(nav).find('.pages li:has(ul)').addClass('expanded').find('ul:first').hide();
    }

    // Show navigation div
    $('.booktreeplus img.booktreeplus-navigation').click(function() {
      if (Drupal.settings.booktreeplus['cookie'])
        $.cookie('booktreeplus-' + Drupal.settings.booktreeplus['bid'] + '-icon', 'No', { path: '/', expires: 0 });
      $(this).parents('.booktreeplus').find('div.booktreeplus-navigation').show();
      $(this).hide();
      return;
    });

    // Hide navigation div
    $('.booktreeplus img.booktreeplus-close').click(function() {
      if (Drupal.settings.booktreeplus['cookie'])
        $.cookie('booktreeplus-' + Drupal.settings.booktreeplus['bid'] + '-icon', 'Yes', { path: '/', expires: 0 });
      $(this).parents('.booktreeplus').find('img.booktreeplus-navigation').show();
      $(this).parents('.booktreeplus').find('div.booktreeplus-navigation').hide();
      return;
    });

    // Expand/collapse sub-folder when clicking parent folder.
    $('.booktreeplus .pages li:has(ul)').click(function(e) {
      // A link was clicked, so don't mess with the folders.
      if ($(e.target).is('a')) {
        return;
      }
      // If multiple folders are not allowed, collapse non-parent folders.
      if (!$(this).parents('.booktreeplus').hasClass('multi')) {
        $(this).parents('.pages').find('li:has(ul)').not($(this).parents()).not($(this)).removeClass('expanded').find('ul:first').hide('fast');
      }
      // Expand.
      if (!$(this).hasClass('expanded')) {
        $(this).addClass('expanded').find('ul:first').show('fast');
      }
      // Collapse.
      else {
        $(this).removeClass('expanded').find('ul:first').hide('fast');
      }
      // Prevent collapsing parent folders.
      return false;
    });

    // Expand/collapse all when clicking controls.
    $('.booktreeplus .controls a').click(function() {
      if ($(this).hasClass('expand')) {
        if (Drupal.settings.booktreeplus['cookie'])
          $.cookie('booktreeplus-' + Drupal.settings.booktreeplus['bid'] + '-expand', 'Yes', { path: '/', expires: 0 });
        $(this).parents('.booktreeplus').find('.pages li:has(ul)').addClass('expanded').find('ul:first').show('fast');
      }
      else {
        if (Drupal.settings.booktreeplus['cookie'])
          $.cookie('booktreeplus-' + Drupal.settings.booktreeplus['bid'] + '-expand', null);
        $(this).parents('.booktreeplus').find('.pages li:has(ul)').removeClass('expanded').find('ul:first').hide('fast');
      }
      return false;
    });

  }

};

})(jQuery);